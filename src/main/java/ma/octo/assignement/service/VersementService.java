package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.NonValidVirementException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.web.VirementController;


@Service
@Transactional
public class VersementService {

	 @Autowired
	 private VersementRepository versementRepository;
	 public static final BigDecimal MONTANT_MAXIMAL = BigDecimal.valueOf(10000);
	 Logger LOGGER = LoggerFactory.getLogger(VirementController.class);
	    @Autowired
	    private CompteService compteService;

	public List<Versement> findAll() {
		return versementRepository.findAll();
	}
	
	 public boolean VersementValid(VersementDto versementDto) throws CompteNonExistantException, NonValidVirementException, TransactionException {
		  Compte compteBeneficiaire = compteService.findByNrCompte(versementDto.getNrCompteBeneficiaire());

		 if (compteBeneficiaire == null) {
	            throw new CompteNonExistantException("Compte Non existant");
	        }

	        if (versementDto.getMontantVirement().equals(null)) {
	            throw new TransactionException("Montant vide");
	        } else if (versementDto.getMontantVirement().compareTo(BigDecimal.TEN)==-1) {
	            throw new TransactionException("Montant minimal de virement non atteint");
	        } else if (versementDto.getMontantVirement().compareTo(MONTANT_MAXIMAL)==1) {
	            throw new TransactionException("Montant maximal de virement dépassé");
	        }

	        if (versementDto.getMotif().length() < 0) {
	            throw new TransactionException("Motif vide");
	        }
		 return true;
		 
	 }
	
	 public void save(Versement versement) {
		 versementRepository.save(versement);
	 }
}
