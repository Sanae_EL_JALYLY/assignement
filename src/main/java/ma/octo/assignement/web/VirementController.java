package ma.octo.assignement.web;

import ma.octo.assignement.AssignementApplication;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.NonValidVirementException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.UserNameNotUniqueException;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AutiService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.UtilisateurService;
import ma.octo.assignement.service.VirementService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;


import java.math.BigDecimal;
import java.util.List;

@RestController(value = "/virements")
public class VirementController {

    public static final BigDecimal MONTANT_MAXIMAL = BigDecimal.valueOf(10000);

    Logger LOGGER = LoggerFactory.getLogger(VirementController.class);

    @Autowired
    private CompteService compteService;
    @Autowired
    private VirementService virementService;

    @Autowired
    private AutiService monservice;
    
    
    @GetMapping("virements")
    List<VirementDto> loadAll() {
    	List<Virement> all = virementService.findAll();
       if (CollectionUtils.isEmpty(all)) {
             return null;
         } else {
       	  return VirementMapper.map(all);
         }
    }

    @PostMapping("/executerVirements")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody VirementDto virementDto)
            throws Exception {
        Compte compteEmetteur = compteService.findByNrCompte(virementDto.getNrCompteEmetteur());
        Compte compteRecepteur = compteService.findByNrCompte(virementDto.getNrCompteBeneficiaire());

        if(virementService.VirementValid(virementDto)) {
        	 compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(virementDto.getMontantVirement()));
             compteService.save(compteEmetteur);

             compteRecepteur.setSolde(new BigDecimal(compteRecepteur.getSolde().intValue() + virementDto.getMontantVirement().intValue()));
             compteService.save(compteRecepteur);

             
             Virement virement = new Virement();
             virement.setDateExecution(virementDto.getDate());
             virement.setCompteBeneficiaire(compteRecepteur);
             virement.setCompteEmetteur(compteEmetteur);
             virement.setMontantVirement(virementDto.getMontantVirement());

             virementService.save(virement);

             monservice.auditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                             .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                             .toString());
        }

       
    }

    }
    
