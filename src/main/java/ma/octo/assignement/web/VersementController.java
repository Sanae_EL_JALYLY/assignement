package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.NonValidVirementException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.service.AutiService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.UtilisateurService;
import ma.octo.assignement.service.VersementService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController(value = "/versements")
public class VersementController {
	 
	 public static final BigDecimal MONTANT_MAXIMAL = BigDecimal.valueOf(10000);

	    Logger LOGGER = LoggerFactory.getLogger(VirementController.class);

	    @Autowired
	    private CompteService compteService;
	    @Autowired
	    private VersementService versementService;

	    @Autowired
	    private AutiService monservice;
	    
	    
	    @Autowired
	    private CompteDto compteDto;
	    
	    @GetMapping("versements")
	    List<VersementDto> loadAll() {
	    	List<Versement> all = versementService.findAll();
	       if (CollectionUtils.isEmpty(all)) {
	             return null;
	         } else {
	       	  return VersementMapper.map(all);
	         }
	    }

	    @PostMapping("/executeVersements")
	    @ResponseStatus(HttpStatus.CREATED)
	    public void createPayement(@RequestBody VersementDto versementDto) throws Exception{
	        
	    	Compte c1 = compteService.findByNrCompte(versementDto.getNrCompteBeneficiaire());

	       if(versementService.VersementValid(versementDto)) {

	        c1.setSolde(c1.getSolde().add(versementDto.getMontantVirement()));
	        compteService.save(c1);

	        Versement versement = new Versement();
	        versement.setDateExecution(versementDto.getDate());
	        versement.setCompteBeneficiaire(c1);
	        versement.setMotifVersement(versementDto.getMotif());
	        versement.setMontantVirement(versementDto.getMontantVirement());

	        versementService.save(versement);

	        monservice.auditVersement("Versement depuis " + versementDto.getNom_prenom_emetteur() + " vers " + versementDto
	                        .getNrCompteBeneficiaire() + " d'un montant de " + versementDto.getMontantVirement()
	                        .toString());
	    }
	    }
	}
