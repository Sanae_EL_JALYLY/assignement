package ma.octo.assignement.mapper;


import java.util.List;import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.VirementService;

public class VirementMapper {

    private static VirementDto virementDto;
    @Autowired
    private static VirementService virementService;
    @Autowired
    private static VirementRepository virementRepository;
    @Autowired
    private static CompteRepository compteRepository;
    private static Virement virement;

    public static VirementDto map(Virement virement) {
        virementDto = new VirementDto();
        virementDto.setId(virement.getId());
        virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
        virementDto.setNrCompteBeneficiaire(virement.getCompteBeneficiaire().getNrCompte());
        virementDto.setMontantVirement(virement.getMontantVirement());
        virementDto.setDate(virement.getDateExecution());
        virementDto.setMotif(virement.getMotifVirement());

        return virementDto;

    }
    
    public static List<VirementDto> map(List<Virement> virement){
    	
    	return virement.stream().map(x->map(x)).collect(Collectors.toList());
    
    }
    public static Virement map(VirementDto virementDto) {
        virement = new Virement();
        virement.setId(virementDto.getId());
        virement.setCompteEmetteur(compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur()));
        virement.setCompteBeneficiaire(compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire()));
        virement.setMontantVirement(virementDto.getMontantVirement());
        virement.setDateExecution(virementDto.getDate());
        virement.setMotifVirement(virementDto.getMotif());

        return virement;

    }
    
    
    
    
    
}
