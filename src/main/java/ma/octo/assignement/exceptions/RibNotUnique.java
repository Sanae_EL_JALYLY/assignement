package ma.octo.assignement.exceptions;

public class RibNotUnique extends Exception{
	 private static final long serialVersionUID = 1L;

	  public RibNotUnique() {
	  }

	  public RibNotUnique(String message) {
	    super(message);
	  }
	}

