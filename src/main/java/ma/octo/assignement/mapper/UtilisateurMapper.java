package ma.octo.assignement.mapper;

import java.util.List;
import java.util.stream.Collectors;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.UtilisateurDto;


public class UtilisateurMapper {

	private static  UtilisateurDto utilisateurDto;
	private static  Utilisateur utilisateur;
	
	 public static  UtilisateurDto map(Utilisateur utilisateur) {
	        utilisateurDto = new UtilisateurDto();
	        utilisateurDto.setUsername(utilisateur.getUsername());
	        utilisateurDto.setFirstname(utilisateur.getFirstname());
	        utilisateurDto.setLastname(utilisateur.getLastname());
	        utilisateurDto.setGender(utilisateur.getGender());
	        utilisateurDto.setBirthdate(utilisateur.getBirthdate());
	        return utilisateurDto;

	    }
	   
	   public static List<UtilisateurDto> map(List<Utilisateur> utilisateur){
	    	
	    	return utilisateur.stream().map(x->map(x)).collect(Collectors.toList());
	    
	    }
	   
}
