package ma.octo.assignement.exceptions;

public class NrCompteNotUniqueException extends Exception {
	
	 private static final long serialVersionUID = 1L;
	
	public NrCompteNotUniqueException() {
		
	}
	
	public NrCompteNotUniqueException(String message) {
		super(message);
	}

}
