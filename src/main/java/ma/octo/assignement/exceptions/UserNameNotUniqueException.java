package ma.octo.assignement.exceptions;

public class UserNameNotUniqueException extends Exception{
	
	 private static final long serialVersionUID = 1L;
	 
	 public UserNameNotUniqueException() {
		 
	 }
	 
	 public UserNameNotUniqueException(String message) {
		    super(message);
		  }

}
