package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

@AutoConfigureTestDatabase(replace=Replace.NONE)
@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VirementRepositoryTest {

  @Autowired
  private VirementRepository virementRepository;


  @Test
  public void save() {
	  Virement virement=new Virement();
	  virement.setCompteBeneficiaire(new Compte());
	  virement.setCompteEmetteur(new Compte());
	  virement.setDateExecution(new Date(16/8/1999));
	  virement.setMontantVirement(BigDecimal.valueOf(123456));
	  virement.setMotifVirement("Motif Test");
	  Virement savedvirement=virementRepository.save(virement);
	  
	  assertNotNull(savedvirement);
  }
  
  @Test
  public void findAll() {
	  
	  List<Virement> virements=virementRepository.findAll();
	  assertThat(virements).size().isGreaterThan(0);

  }

  @Test
  public void delete() {
	  long id=5;
	  boolean existbeforedelete=virementRepository.findById(id).isPresent();
	  virementRepository.deleteById(id);
	  boolean existafterdelete=virementRepository.findById(id).isPresent();
	assertTrue(existbeforedelete);
	assertFalse(existafterdelete);
	
	
	  
  }
}