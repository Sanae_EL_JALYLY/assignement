package ma.octo.assignement.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;

@AutoConfigureTestDatabase(replace=Replace.NONE)
@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VersementRepositoryTest {
	 @Autowired
	  private VersementRepository versementRepository;
	 
	 @Test
	  public void findAll() {
		  List<Versement> versements=versementRepository.findAll();
		  assertThat(versements).size().isGreaterThan(0);

	  }

	  @Test
	  public void save() {
		  Versement versement=new Versement();
		  versement.setCompteBeneficiaire(new Compte());
		  versement.setNom_prenom_emetteur("ELJALYLY SANAE");
		  versement.setDateExecution(new Date(16/11/2021));
		  versement.setMontantVirement(BigDecimal.valueOf(123));
		  versement.setMotifVersement("Motif Test");
		  Versement savedversement=versementRepository.save(versement);
		  
		  assertNotNull(savedversement);
	  }

}
