package ma.octo.assignement.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.exceptions.UserNameNotUniqueException;
import ma.octo.assignement.repository.UtilisateurRepository;

@Service
@Transactional
public class UtilisateurService {
	
	 @Autowired
	 private UtilisateurRepository utilisateurRepository;
	 
	 public List<Utilisateur> findAll(){
		 return utilisateurRepository.findAll();
	 }
	 
	 public boolean userValid(Utilisateur utilisateur) throws UserNameNotUniqueException {
		 if(utilisateurRepository.findByUsername(utilisateur.getUsername())!=null) {
			 throw new UserNameNotUniqueException("User Name Existe deja");
		 }
		 return true;
	 }
	 
	 public void save(Utilisateur user){
		 utilisateurRepository.save(user);
		 
	 }

	 
}
