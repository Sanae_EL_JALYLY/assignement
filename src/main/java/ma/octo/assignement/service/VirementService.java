package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.NonValidVirementException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.web.VirementController;

@Service
@Transactional
public class VirementService {

	 @Autowired
	 private VirementRepository virementRepository;
	 public static final BigDecimal MONTANT_MAXIMAL = BigDecimal.valueOf(10000);
	 Logger LOGGER = LoggerFactory.getLogger(VirementController.class);
	    @Autowired
	    private CompteService compteService;
	 
	public List<Virement> findAll(){
		 return virementRepository.findAll();
	 }
	 
	 public void save(Virement virement) {
		 virementRepository.save(virement);
	 }
	 
	
	 public boolean VirementValid(VirementDto virementDto) throws CompteNonExistantException, NonValidVirementException, TransactionException {
		  Compte compteEmetteur = compteService.findByNrCompte(virementDto.getNrCompteEmetteur());
	        Compte compteRecepteur = compteService.findByNrCompte(virementDto.getNrCompteBeneficiaire());

		 if (compteEmetteur == null) {
	            System.out.println("Compte Non existant");
	            throw new CompteNonExistantException("Compte Non existant");
	        }

	        if (compteRecepteur == null) {
	            System.out.println("Compte Non existant");
	            throw new CompteNonExistantException("Compte Non existant");
	        }
		if(virementDto.getNrCompteBeneficiaire().equals(virementDto.getNrCompteEmetteur())) {
			  System.out.println("Virement Non valide ");
	          throw new NonValidVirementException("Virement Non valide");
			}

	        if (virementDto.getMontantVirement().equals(null)) {
	            throw new TransactionException("Montant vide");
	        } else if (virementDto.getMontantVirement().compareTo(BigDecimal.TEN)==-1) {
	            throw new TransactionException("Montant minimal de virement non atteint");
	        } else if (virementDto.getMontantVirement().compareTo(MONTANT_MAXIMAL)==1) {
	            throw new TransactionException("Montant maximal de virement dépassé");
	        }

	        if (virementDto.getMotif().length() < 0) {
	            throw new TransactionException("Motif vide");
	        }

	        if (compteEmetteur.getSolde().subtract(virementDto.getMontantVirement()).equals(BigDecimal.ZERO) ) {
	            LOGGER.error("Solde insuffisant pour l'utilisateur");
	        }
		 return true;
		 
	 }
	 
}
