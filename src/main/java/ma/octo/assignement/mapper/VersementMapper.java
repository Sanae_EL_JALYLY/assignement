package ma.octo.assignement.mapper;

import java.util.List;
import java.util.stream.Collectors;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;

public class VersementMapper {
	private static VersementDto versementDto;
    private static Versement versement;

    
    public static VersementDto map(Versement versement) {
        versementDto = new VersementDto();
        versementDto.setNrCompteBeneficiaire(versement.getCompteBeneficiaire().getNrCompte());
        versementDto.setMontantVirement(versement.getMontantVirement());
        versementDto.setMotif(versement.getMotifVersement());
        versementDto.setNom_prenom_emetteur(versement.getNom_prenom_emetteur());
        versementDto.setDate(versement.getDateExecution());

        return versementDto;

    }
    
    public static List<VersementDto> map(List<Versement> versement){
    	
    	return versement.stream().map(x->map(x)).collect(Collectors.toList());
    
    }
}
