package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.service.AutiService;
import ma.octo.assignement.service.UtilisateurService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController(value = "/utilisateurs")
public class UtilisateurController {
	
	 @Autowired
	    private UtilisateurService utilisateurService;
	

    @GetMapping("utilisateurs")
    List<UtilisateurDto> loadAllUtilisateur() {
        List<Utilisateur> all = utilisateurService.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return UtilisateurMapper.map(all);
        }
    }

}
