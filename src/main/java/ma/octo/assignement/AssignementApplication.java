package ma.octo.assignement;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.NrCompteNotUniqueException;
import ma.octo.assignement.exceptions.RibNotUnique;
import ma.octo.assignement.exceptions.UserNameNotUniqueException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.UtilisateurService;
import ma.octo.assignement.service.VirementService;
import ma.octo.assignement.web.VersementController;
import ma.octo.assignement.web.VirementController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
public class AssignementApplication implements CommandLineRunner {

	@Autowired
	private CompteService compteService;
	@Autowired
	private UtilisateurService utilisateurService;
	@Autowired
	private VirementService virementService;
	@Autowired
	private VirementController virementController;
	@Autowired
	private VersementController versementController;
	public static void main(String[] args) {
		SpringApplication.run(AssignementApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Male");
		 if(utilisateurService.userValid(utilisateur1)) {
			 utilisateurService.save(utilisateur1);
		 }


		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur2.setGender("Female");
		 if(utilisateurService.userValid(utilisateur2)) {
			 utilisateurService.save(utilisateur2);
		 }

		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);
		if(compteService.ValideCompte(compte1)){
			 compteService.save(compte1);
			 System.out.println("Compte Creer");
		 }

		Compte compte2 = new Compte();
		compte2.setNrCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUtilisateur(utilisateur2);
		if(compteService.ValideCompte(compte2)){
			 compteService.save(compte2);
			 System.out.println("Compte Creer");
		 }
		Virement virement = new Virement();
		virement.setMontantVirement(BigDecimal.TEN);
		virement.setCompteBeneficiaire(compte2);
		virement.setCompteEmetteur(compte1);
		virement.setDateExecution(new Date());
		virement.setMotifVirement("Assignment 2021");
		virementService.save(virement);
		
		VirementDto virementDto =VirementMapper.map(virement);
		virementController.createTransaction(virementDto);
		
		Versement versement =new Versement();
		versement.setCompteBeneficiaire(compte2);
		versement.setDateExecution(new Date());
		versement.setMontantVirement(BigDecimal.TEN);
		versement.setMotifVersement("Versement Assignmrnt 2021");
		versement.setNom_prenom_emetteur("ELJALYLY SANAE");
		
		VersementDto versementDto=VersementMapper.map(versement);
		versementController.createPayement(versementDto);
		
	}
}
