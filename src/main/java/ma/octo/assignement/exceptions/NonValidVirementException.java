package ma.octo.assignement.exceptions;

public class NonValidVirementException extends Exception {

	 private static final long serialVersionUID = 1L;
      
	public NonValidVirementException() {
		
	}
	public NonValidVirementException(String message) {
		super(message);
	}
}
