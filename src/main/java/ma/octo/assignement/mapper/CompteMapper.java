package ma.octo.assignement.mapper;

import java.util.List;
import java.util.stream.Collectors;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.VirementDto;

public class CompteMapper {
	
	 private static  CompteDto compteDto;
	 private static  Compte compte;
	 
	 
	   public static  CompteDto map(Compte compte) {
	        compteDto = new CompteDto();
	        compteDto.setNrCompte(compte.getNrCompte());
	        compteDto.setRib(compte.getRib());
	        compteDto.setSolde(compte.getSolde());
	        compteDto.setUsername(compte.getUtilisateur().getUsername());
	        return compteDto;

	    }
	   
	   public static List<CompteDto> map(List<Compte> compte){
	    	
	    	return compte.stream().map(x->map(x)).collect(Collectors.toList());
	    
	    }
	   

}
