package ma.octo.assignement.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.exceptions.NrCompteNotUniqueException;
import ma.octo.assignement.exceptions.RibNotUnique;
import ma.octo.assignement.repository.CompteRepository;

@Service
@Transactional
public class CompteService {
	
	 @Autowired
	 private CompteRepository compteRepository;
	 
	 
	 public List<Compte> findAll(){
		 return compteRepository.findAll();
	 }
	 
	 public Compte findByNrCompte(String nrCompte) {
		return  compteRepository.findByNrCompte(nrCompte);
	 }
	 
	 public Compte findByRib(String rib) {
		 return  compteRepository.findByRib(rib);
	 }
	 
	 public boolean ValideCompte(Compte compte) throws NrCompteNotUniqueException, RibNotUnique {
		 if(compteRepository.findByNrCompte(compte.getNrCompte())!=null) {
			 throw new NrCompteNotUniqueException("Numero de compte existe deja");
		 }else if(compteRepository.findByRib(compte.getRib())!=null) {
			 throw new RibNotUnique("RIB existe deja");
		 }else{
			 return true;
		 }
		 
		 
	 }
	 
	 public void save(Compte compte) {
		 compteRepository.save(compte);
	 }
}
